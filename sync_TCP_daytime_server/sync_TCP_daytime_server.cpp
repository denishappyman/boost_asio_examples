//
// server.cpp
// ~~~~~~~~~~
//
// Copyright (c) 2003-2019 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#include <ctime>
#include <iostream>
#include <string>
#include <boost/asio.hpp>

using boost::asio::ip::tcp;

std::string make_daytime_string()
{
  using namespace std; // For time_t, time and ctime;
  time_t now = time(0);
  return ctime(&now);
}

int main()
{
  try
  {
    boost::asio::io_context io_context;

    tcp::acceptor acceptor(io_context, tcp::endpoint(tcp::v4(), 13));

    for (;;)
    {
      using namespace std;
      cout << "loop start" << endl;
      tcp::socket socket(io_context);
      cout  << "socket" << endl;
      acceptor.accept(socket);
      cout << "accept" << endl;
      std::string message = make_daytime_string();
      cout << message << endl;
      boost::asio::steady_timer t(io_context, boost::asio::chrono::seconds(5));
      cout << "timer t" << endl;
      t.wait();
      cout << "t.wait()" << endl;
      boost::system::error_code ignored_error;
      boost::asio::write(socket, boost::asio::buffer(message), ignored_error);
      cout << "asio::write, loop end" << endl;
    }
  }
  catch (std::exception& e)
  {
    std::cerr << e.what() << std::endl;
  }

  return 0;
}